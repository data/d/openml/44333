{
  "data_set_description": {
    "collection_date": "30-09-2022",
    "creator": "Ihsan Ullah",
    "default_target_attribute": "CATEGORY",
    "description": "## **Meta-Album RSICB Dataset (Extended)**\n***\nRSICB128 dataset (https://github.com/lehaifeng/RSI-CB) covers 45 scene categories, assembling in total 36 000 images of resolution 128x128 px. The data authors select various locations around the world, and follow China's landuse classification standard. This collection has 2-level label hierarchy with 6 super-categories: agricultural land, construction land and facilities, transportation and facilities, water and water conservancy facilities, woodland, and other lands. The preprocessed version of RSICB is created by resizing the images into 128x128 px using an anti-aliasing filter.  \n\n\n\n### **Dataset Details**\n![](https://meta-album.github.io/assets/img/samples/RSICB.png)\n\n**Meta Album ID**: REM_SEN.RSICB  \n**Meta Album URL**: [https://meta-album.github.io/datasets/RSICB.html](https://meta-album.github.io/datasets/RSICB.html)  \n**Domain ID**: REM_SEN  \n**Domain Name**: Remote Sensing  \n**Dataset ID**: RSICB  \n**Dataset Name**: RSICB  \n**Short Description**: Remote sensing dataset  \n**\\# Classes**: 45  \n**\\# Images**: 36707  \n**Keywords**: remote sensing, satellite image, aerial image, land cover  \n**Data Format**: images  \n**Image size**: 128x128  \n\n**License (original data release)**: Open for research purposes  \n**License (Meta-Album data release)**: CC BY-NC 4.0  \n**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  \n\n**Source**: RSI-CB: A Large Scale Remote Sensing Image Classification Benchmark via Crowdsource Data  \n**Source URL**: https://github.com/lehaifeng/RSI-CB  \n  \n**Original Author**: Haifeng Li, Xin Dou, Chao Tao, Zhixiang Hou, Jie Chen, Jian Peng, Min Deng, Ling Zhao  \n**Original contact**: lihaifeng@csu.edu.cn  \n\n**Meta Album author**: Phan Anh VU  \n**Created Date**: 01 March 2022  \n**Contact Name**: Ihsan Ullah  \n**Contact Email**: meta-album@chalearn.org  \n**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  \n\n\n\n### **Cite this dataset**\n```\n@article{li2020RSI-CB,\n    title={RSI-CB: A Large-Scale Remote Sensing Image Classification Benchmark Using Crowdsourced Data},\n    author={Li, Haifeng and Dou, Xin and Tao, Chao and Wu, Zhixiang and Chen, Jie and Peng, Jian and Deng, Min and Zhao, Ling},\n    journal={Sensors},\n    DOI = {doi.org/10.3390/s20061594},\n    year={2020},\n    volume = {20},\n    number = {6},\n    pages = {1594},\n    type = {Journal Article}\n}\n```\n\n\n### **Cite Meta-Album**\n```\n@inproceedings{meta-album-2022,\n        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},\n        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},\n        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},\n        url = {https://meta-album.github.io/},\n        year = {2022}\n    }\n```\n\n\n### **More**\nFor more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  \nFor details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  \nSupporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  \nMeta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  \n\n\n\n### **Other versions of this dataset**\n[[Micro]](https://www.openml.org/d/44315)  [[Mini]](https://www.openml.org/d/44300)",
    "description_version": "2",
    "file_id": "22111047",
    "format": "arff",
    "id": "44333",
    "language": "English",
    "licence": "CC BY-NC 4.0",
    "md5_checksum": "1a9b5af1a358a0f38282a6a2ab3351d9",
    "minio_url": "https://data.openml.org/datasets/0004/44333/dataset_44333.pq",
    "name": "Meta_Album_RSICB_Extended",
    "parquet_url": "https://data.openml.org/datasets/0004/44333/dataset_44333.pq",
    "processing_date": "2022-11-08 18:28:15",
    "status": "active",
    "upload_date": "2022-11-08T18:28:07",
    "url": "https://api.openml.org/data/v1/download/22111047/Meta_Album_RSICB_Extended.arff",
    "version": "1",
    "visibility": "public"
  }
}